<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/about-us', function () {
    //return view('welcome');
    echo "about us";
});

Route::get('/contact-us', function () {
    return view('contact_us');
    /*echo "contact us";*/
});

Route::get('/gallary', function () {
    return view('gallary');
    /*echo "contact us";*/
});

Route::get('/listing-catalogue', function () {
    return view('listing_catalogue');
    /*echo "contact us";*/
});


Route::get('/listing-collections', function () {
    return view('listing_collections');
    /*echo "contact us";*/
});

Route::get('/404', function () {
    return view('home');
    /*echo "contact us";*/
});

Route::get('/about', function () {
    return view('about');
    /*echo "contact us";*/
});

Route::get('/blog-gallery-post', function () {
    return view('blog_gallary_post');
    /*echo "contact us";*/
});

Route::get('/blog-grid', function () {
    return view('blog_grid');
    /*echo "contact us";*/
});

Route::get('/blog-listing-without-sidebar', function () {
    return view('blog_listing_without_sidebar ');
    /*echo "contact us";*/
});

Route::get('/blog-listing-with-sidebar', function () {
    return view('blog_listing_with_sidebar ');
    /*echo "contact us";*/
});

Route::get('/blog-masonry', function () {
    return view('blog_masonry');
    /*echo "contact us";*/
});

Route::get('/blog-standart-post', function () {
    return view('blog_standart_post');
    /*echo "contact us";*/
});

Route::get('/blog-text-post', function () {
    return view('blog_text_post');
    /*echo "contact us";*/
});

Route::get('/blog-video-post', function () {
    return view('blog_video_post');
    /*echo "contact us";*/
});

Route::get('/cart-01', function () {
    return view('cart_01');
    /*echo "contact us";*/
});
Route::get('/cart-02', function () {
    return view('cart_02');
    /*echo "contact us";*/
});
Route::get('/checkout-01', function () {
    return view('checkout_01');
    /*echo "contact us";*/
});

Route::get('/checkout-02', function () {
    return view('checkout_02');
    /*echo "contact us";*/
});

Route::get('/coming-soon', function () {
    return view('coming_soon');
    /*echo "contact us";*/
});

Route::get('/compare', function () {
    return view('compare');
    /*echo "contact us";*/
});

Route::get('/contacts', function () {
    return view('contacts');
    /*echo "contact us";*/
});
Route::get('/elements-accordion', function () {
    return view('elements-accordion');
    /*echo "contact us";*/
});

Route::get('/elements-banners', function () {
    return view('element_banners');
    /*echo "contact us";*/
});
Route::get('/elements-buttons', function () {
    return view('element_buttons');
    /*echo "contact us";*/
});
Route::get('/elements-icon-box', function () {
    return view('element_icon_box');
    /*echo "contact us";*/
});
Route::get('/elements-maps', function () {
    return view('element_maps');
    /*echo "contact us";*/
});
Route::get('/elements-products', function () {
    return view('element-products');
    /*echo "contact us";*/
});
Route::get('/elements-progress-bar', function () {
    return view('element-progress-bar');
    /*echo "contact us";*/
});
Route::get('/elements-tabs', function () {
    return view('element-tabs');
    /*echo "contact us";*/
});
Route::get('/elements-toggles', function () {
    return view('element-toggles');
    /*echo "contact us";*/
});
Route::get('/elements-typography', function () {
    return view('element-typgaphy');
    /*echo "contact us";*/
});

Route::get('/empty-category', function () {
    return view('empty-category');
    /*echo "contact us";*/
});

Route::get('/empty-compare', function () {
    return view('empty-compare');
    /*echo "contact us";*/
});

Route::get('/empty-search', function () {
    return view('empty-search');
    /*echo "contact us";*/
});

Route::get('/empty-shopping-cart', function () {
    return view('empty-shopping-cart');
    /*echo "contact us";*/
});
Route::get('/empty-wishlist', function () {
    return view('empty-wishlist');
    /*echo "contact us";*/
});
Route::get('/faq', function () {
    return view('faq');
    /*echo "contact us";*/
});
Route::get('/gallery', function () {
    return view('gallery');
    /*echo "contact us";*/
});
Route::get('/gallery-3-thumbs', function () {
    return view('gallery-3-thumbs');
    /*echo "contact us";*/
});
Route::get('/gallery-4-thumbs', function () {
    return view('gallery-4-thumbs');
    /*echo "contact us";*/
});
Route::get('/gallery-masonry', function () {
    return view('gallery-masonry');
    /*echo "contact us";*/
});
Route::get('/index', function () {
    return view('index');
    /*echo "contact us";*/
});
Route::get('/index-02', function () {
    return view('index-02');
    /*echo "contact us";*/
});
Route::get('/index-03', function () {
    return view('index-03');
    /*echo "contact us";*/
});
Route::get('/index-04', function () {
    return view('index-04');
    /*echo "contact us";*/
});
Route::get('/index-05', function () {
    return view('index-05');
    /*echo "contact us";*/
});
Route::get('/index-06', function () {
    return view('index-06');
    /*echo "contact us";*/
});
Route::get('/index-07', function () {
    return view('index-07');
    /*echo "contact us";*/
});
Route::get('/index-08', function () {
    return view('index-08');
    /*echo "contact us";*/
});
Route::get('/index-09', function () {
    return view('index-09');
    /*echo "contact us";*/
});
Route::get('/index-10', function () {
    return view('index-10');
    /*echo "contact us";*/
});
Route::get('/index-11', function () {
    return view('index-11');
    /*echo "contact us";*/
});
Route::get('/index-12', function () {
    return view('index-12');
    /*echo "contact us";*/
});
Route::get('/listing', function () {
    return view('listing');
    /*echo "contact us";*/
});

Route::get('/listing-big-reviews', function () {
    return view('listing_big_reviwes');
    /*echo "contact us";*/
});

Route::get('/listing-catalogue', function () {
    return view('listing_catalogue');
    /*echo "contact us";*/
});

Route::get('/listing-collections', function () {
    return view('listing_collections');
    /*echo "contact us";*/
});

Route::get('/listing-fullwidth', function () {
    return view('listing_fullwidth');
    /*echo "contact us";*/
});

Route::get('/listing-right_sidebar', function () {
    return view('listing_right_sidebar');
    /*echo "contact us";*/
});

Route::get('/listing-with_custom_html_block', function () {
    return view('listing_with_custom_html_block');
    /*echo "contact us";*/
});

Route::get('/listing-with_sidebar', function () {
    return view('listing_with_sidebar');
    /*echo "contact us";*/
});

Route::get('/listing-without_columns', function () {
    return view('listing_without_columns');
    /*echo "contact us";*/
});

Route::get('/login', function () {
    return view('login');
    /*echo "contact us";*/
});

Route::get('/my-account', function () {
    return view('my_account');
    /*echo "contact us";*/
});

Route::get('/product', function () {
    return view('product');
    /*echo "contact us";*/
});

Route::get('/product-grouped', function () {
    return view('product_grouped');
    /*echo "contact us";*/
});

Route::get('/product-on-sale', function () {
    return view('product_on_sale');
    /*echo "contact us";*/
});

Route::get('/product-out-of-stock', function () {
    return view('product_out_of_stock');
    /*echo "contact us";*/
});

Route::get('/product-simple-variant-1', function () {
    return view('product_simple_variant_1');
    /*echo "contact us";*/
});

Route::get('/product-simple-variant-2', function () {
    return view('product_simple_variant_2');
    /*echo "contact us";*/
});
Route::get('/product-variable', function () {
    return view('product_variable');
    /*echo "contact us";*/
});

Route::get('/register', function () {
    return view('register');
    /*echo "contact us";*/
});

Route::get('/services', function () {
    return view('services');
    /*echo "contact us";*/
});

Route::get('/sitemap', function () {
    return view('sitemap');
    /*echo "contact us";*/
});

Route::get('/wishlist', function () {
    return view('wishlist');
    /*echo "contact us";*/
});

